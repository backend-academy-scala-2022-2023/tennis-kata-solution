package tennis

class TennisGame3(val playerOneName: String, val playerTwoName: String)
    extends TennisGame {
  private val playerOne = new Player(playerOneName)
  private val playerTwo = new Player(playerTwoName)

  private val minimumPointsToWin = 4
  private val deuceThreshold = 3
  private val minimumScoreDifferenceToWin = 2

  def calculateScore(): String =
    if (equalPoints)
      if (playerOne.points >= deuceThreshold)
        "Deuce"
      else
        playerOne.score + "-All"
    else if (!advantageStage)
      playerOne.score + "-" + playerTwo.score
    else if (scoreDifference < minimumScoreDifferenceToWin)
      "Advantage " + leader
    else
      "Win for " + leader

  private def advantageStage: Boolean = playerOne.points >= minimumPointsToWin || playerTwo.points >= minimumPointsToWin

  private def equalPoints: Boolean = playerOne.points == playerTwo.points

  private def leader: String =
    if (playerOne.points > playerTwo.points) playerOneName
    else playerTwoName

  private def scoreDifference: Int = Math.abs(playerOne.points - playerTwo.points)


  def wonPoint(playerName: String): Unit = {
    if (playerName == playerOneName)
      playerOne.wonPoint()
    else
      playerTwo.wonPoint()
  }
}

private class Player(val name: String) {
  private var _points = 0

  def score: String = _points match {
    case 0 => "Love"
    case 1 => "Fifteen"
    case 2 => "Thirty"
    case 3 => "Forty"
    case _ => "Advantage/Win"
  }

  def points: Int = _points

  def wonPoint(): Unit = {
    _points += 1
  }
}
